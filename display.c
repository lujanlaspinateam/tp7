/*
===============================================================================
 Name        : display.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
===============================================================================
*/

#define AddrPINSEL0 0x4002C000
#define AddrPINSEL1 0x4002C004
#define AddrPINSEL4 0x4002C010
#define AddrFIO0DIR 0x2009C000
#define AddrFIO0SET 0x2009C018
#define AddrFIO0CLR 0x2009C01C
#define AddrFIO0PIN 0x2009C014
#define AddrIO2IntEnR 0x400280B0 //registros asociados a las
#define AddrIO2IntEnF 0x400280B4 //interrupciones por GPIO2
#define AddrISER0 0xE000E100
#define AddrIO2IntStatR 0x400280A4
#define AddrIO2IntStatF 0x400280A8
#define AddrIO2IntClr 0x400280AC
#define AddrEXTMODE 0x400FC148
#define AddrEXTPOLAR 0x400FC14C
#define AddrEXTINT   0x400fc140

unsigned int volatile *const PINSEL0 = (unsigned int*)AddrPINSEL0;
unsigned int volatile *const PINSEL1 = (unsigned int*)AddrPINSEL1;
unsigned int volatile *const PINSEL4= (unsigned int*)AddrPINSEL4;
unsigned int volatile *const FIO0DIR = (unsigned int*)AddrFIO0DIR;
unsigned int volatile *const FIO0SET = (unsigned int*)AddrFIO0SET;
unsigned int volatile *const FIO0CLR = (unsigned int*)AddrFIO0CLR;
unsigned int volatile *const FIO0PIN = (unsigned int*)AddrFIO0PIN;
unsigned int volatile *const IO2IntEnR = (unsigned int*)AddrIO2IntEnR;
unsigned int volatile *const IO2IntEnF = (unsigned int*)AddrIO2IntEnF;
unsigned int volatile *const ISER0 = (unsigned int*)AddrISER0;
unsigned int volatile *const IO2IntStatR = (unsigned int*)AddrIO2IntStatR;
unsigned int volatile *const IO2IntStatF = (unsigned int*)AddrIO2IntStatF;
unsigned int volatile *const IO2IntClr = (unsigned int*)AddrIO2IntClr;
unsigned int volatile*const EXTMODE= (unsigned int*)AddrEXTMODE;
unsigned int volatile*const EXTPOLAR= (unsigned int*)AddrEXTPOLAR;
unsigned int volatile *const EXTINT=(unsigned int*)AddrEXTINT;

int retardo(unsigned int);
unsigned int tiempo =5000000;
unsigned int tiempo1 =5000000;
static int conteo = 0;
static int conteodisplay = 0;

int main(void){

	*PINSEL4 |=(5<<20);
	*FIO0DIR |= (1<<2); // output P2 pone un 1 sin modificar el resto
	*FIO0DIR &=~(3<<10);
	*EXTMODE |=(1<<1);   //controlo que sea por nivel o por flanco, en este caso
	*EXTMODE |=(1<<0);	//flanco para EINT1, y flanco para EINT0.
	*EXTPOLAR |=(0<<1); // seteo si el nivel es por alto o por bajo, o el flanco por subida o bajada
	*EXTPOLAR |=(1<<0);
	*IO2IntEnR |=(1<<10); //Enable rising edge interrupt for P0.10
	*IO2IntEnF |=(1<<11); //Enable falling edge interrupt for P0.11
	*EXTINT |=(1<<0);     //registro de interrupciones->
	*ISER0 |= (3<<18); //External Interrupt 3 interrupt Enable
	*FIO0DIR |= (1<<6); //segmento A
	*FIO0DIR |= (1<<7);	//segmento B
	*FIO0DIR |= (1<<8);//segmento C
	*FIO0DIR |= (1<<9);  //segmento D
	*FIO0DIR |= (1<<3);  //segmento E
	*FIO0DIR |= (1<<1); //segmento F
	*FIO0DIR |= (1<<5);  //segmento G

	while (1){
		*FIO0SET |= (1<<2);
		retardo(tiempo);				//main loop del led
		*FIO0CLR |= (1<<2);
		retardo(tiempo);

	}
return 0;
}

void EINT0_IRQHandler(void){

	//if((*IO2IntStatR>>10)&1){ //Status of rising edge interrupt for P0.15
		conteo++;
		if (conteo%2 == 0){
	    tiempo=400000;                  //servicio de interrupciones
		}
	    else{
	    	tiempo =900000;
	    }
		*EXTINT |=(1<<0); // borro la bandera de EINT0
	//}
}
	//if((*IO0IntStatF>>16)&1){ //status of FALLING edge interrupt for P0.16

void EINT1_IRQHandler(void){
					int i;
					conteodisplay++;
					//for (i=0;i<10;i++){
						escribir(conteodisplay);
						retardo(tiempo1);
						if(conteodisplay == 9 ){
							*EXTMODE |= (0<<1);
							*EXTPOLAR |=(1<<0);// cambio los registros para pasar a nivel por bajo
							conteodisplay =0;

						}
					//}


		*EXTINT |=(1<<1); //borro la bandera de EINT1
	}


int retardo(unsigned int tiempo){
	unsigned int i;
	for (i=0;i<tiempo;i++);
	return 0;
}




void escribir (int n){  // rutina para mostrar los numeros en el display
	switch(n){
								case 0:
									*FIO0CLR |=(1<<5);//G
									*FIO0SET |= (1<<6);//A
									*FIO0SET |= (1<<7);//B
									*FIO0SET |= (1<<8);//C
									*FIO0SET |= (1<<9);//D
									*FIO0SET |= (1<<3);//E
									*FIO0SET |= (1<<1);//F
									break;
								case 1:
									*FIO0CLR |= (1<<6);
									*FIO0CLR |= (1<<9);
									*FIO0CLR |= (1<<3);
									*FIO0CLR |= (1<<1);
									*FIO0CLR |= (1<<5);
									*FIO0SET |= (1<<7);
									*FIO0SET |= (1<<8);
									break;
								case 2:
									*FIO0CLR |= (1<<8);
									*FIO0CLR |= (1<<1);
									*FIO0SET |= (1<<6);
									*FIO0SET |= (1<<7);
									*FIO0SET |= (1<<5);
									*FIO0SET |= (1<<9);
									*FIO0SET |= (1<<3);
									break;
								case 3:
									*FIO0CLR |= (1<<3);
									*FIO0CLR |= (1<<1);
									*FIO0SET |= (1<<5);
									*FIO0SET |= (1<<6);
									*FIO0SET |= (1<<7);
									*FIO0SET |= (1<<8);
									*FIO0SET |= (1<<9);
									break;
								case 4:
									*FIO0SET |= (1<<5);
									*FIO0SET |= (1<<7);
									*FIO0SET |= (1<<8);
									*FIO0SET |= (1<<1);
									*FIO0CLR |= (1<<3);
									*FIO0CLR |= (1<<9);
									*FIO0CLR |= (1<<6);
									break;
								case 5:
									*FIO0SET |= (1<<6);
									*FIO0SET |= (1<<8);
									*FIO0SET |= (1<<9);
									*FIO0SET |= (1<<5);
									*FIO0SET |= (1<<1);
									*FIO0CLR |= (1<<7);
									*FIO0CLR |= (1<<3);
									break;
								case 6:
									*FIO0SET |= (1<<6);
									*FIO0SET |= (1<<8);
									*FIO0SET |= (1<<9);
									*FIO0SET |= (1<<3);
									*FIO0SET |= (1<<1);
									*FIO0SET |= (1<<5);
									*FIO0CLR |= (1<<7);
									break;
								case 7:
									*FIO0SET |= (1<<6);
									*FIO0SET |= (1<<7);
									*FIO0SET |= (1<<8);
									*FIO0CLR |= (1<<9);
									*FIO0CLR |= (1<<3);
									*FIO0CLR |= (1<<1);
									*FIO0CLR |= (1<<5);
									break;
								case 8:
									*FIO0SET |= (1<<6);
									*FIO0SET |= (1<<7);
									*FIO0SET |= (1<<8);
									*FIO0SET |= (1<<9);
									*FIO0SET |= (1<<3);
									*FIO0SET |= (1<<1);
									*FIO0SET |= (1<<5);
									break;
								case 9:
									*FIO0CLR |= (1<<3);
									*FIO0SET |= (1<<6);
									*FIO0SET |= (1<<7);
									*FIO0SET |= (1<<8);
									*FIO0CLR |= (1<<9);
									*FIO0SET |= (1<<1);
									*FIO0SET |= (1<<5);


								}
}

